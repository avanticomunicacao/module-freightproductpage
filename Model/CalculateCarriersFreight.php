<?php
declare(strict_types=1);
namespace Avanti\FreightProductPage\Model;

use Avanti\FreightProductPage\Api\CalculateFreightInterface;
use Magento\Framework\App\Request\Http as RequestHttp;
use Magento\Catalog\Model\Product;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\TotalsCollector;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Checkout\Model\Session;

class CalculateCarriersFreight implements CalculateFreightInterface
{
    protected $request;
    protected $product;
    protected $quote;
    protected $totalsCollector;
    protected $storeManager;
    protected $session;

    public function __construct(
        RequestHttp $request,
        Product $product,
        Quote $quote,
        TotalsCollector $totalsCollector,
        StoreManagerInterface $storeManager,
        Session $session
    ) {
        $this->request = $request;
        $this->product = $product;
        $this->quote = $quote;
        $this->totalsCollector = $totalsCollector;
        $this->storeManager = $storeManager;
        $this->session = $session;
    }

    /**
     * {@inheritdoc}
     */
    public function getCarriersFreight()
    {
        $productId = $this->request->getParam('productId');
        $zipCode = $this->request->getParam('zipCode');

        if (!isset($zipCode) || !isset($productId)) {
            $result = [
                "status" => "error",
                "message" => "The attribute zipCode or ProductId is empty. Try again."
            ];
            return json_encode($result);
        }
        $this->product->load($productId);
        $leadTime = $this->product->getData('lead_time');
        $this->quote->addProduct($this->product);
        $this->quote->collectTotals();
        $this->quote->save();
        $this->session->setQuoteId($this->quote->getId());

        $shipping = $this->quote->getShippingAddress();
        $shipping->setCountryId('BR');
        $shipping->setPostcode($zipCode);
        $shipping->setCollectShippingRates(true);

        $this->totalsCollector->collectAddressTotals($this->quote, $shipping);
        $rates = $shipping->collectShippingRates()->getAllShippingRates();

        $data = array();
        if (count($rates)) {
            foreach ($rates as $k => $rate) {
                if ($rate->getCode() != 'flatrate_flatrate' && $leadTime != null) {
                    try{
                        $methodTitle = explode(" ", $rate->getMethodTitle());
                        if (is_numeric($methodTitle[4])) {
                            $days = intval($methodTitle[4]);
                            $days = $days + intval($leadTime);
                            $rate->setMethodTitle($methodTitle[0] . " - ". $methodTitle[2] . " " . $methodTitle[3]
                            . " " . $days . " " . $methodTitle[5]);
                        }
                    } catch (\Exception $e) {
                        continue;
                    }
                }

                $data[$k]['title'] = $rate->getMethodTitle();
                $data[$k]['price'] = "R$" . $rate->getPrice();
            }

            $this->session->setQuoteId(null);
            return json_encode($data);
        }

        throw new \Exception('Não foi possivel calcular frete.');
    }
}
