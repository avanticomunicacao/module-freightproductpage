<?php
namespace Avanti\FreightProductPage\Api;

interface CalculateFreightInterface
{
    /**
     * GET Avaliable Carries
     * @return string
     * @throws \Exception
     */
    public function getCarriersFreight();
}
